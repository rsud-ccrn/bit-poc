import React from 'react';
import { render } from '@testing-library/react';
import { BasicDynamicForm } from './dynamic-form.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDynamicForm />);
  const rendered = getByText('hello world!');
  expect(rendered).toBeTruthy();
});
