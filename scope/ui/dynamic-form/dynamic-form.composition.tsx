import React from 'react';
import { DynamicForm } from './dynamic-form';

export const BasicDynamicForm = () => {
  return (
    <DynamicForm
      fields={[
        { field: 'First Name', required: true },
        { field: 'Last Name', required: false },
        { field: 'Email Address', required: true },
      ]}
    />
  );
};
