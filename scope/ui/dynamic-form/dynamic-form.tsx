import React, { ReactNode } from 'react';
import styles from './dynamic-form.module.scss';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import toPairs from 'lodash/toPairs';
import keys from 'lodash/keys';

export type DynamicFormProps = {
  /**
   * a node to be rendered in the special component.
   */
  fields: any;
};

export function DynamicForm({ fields }: DynamicFormProps) {
  const fieldsArray: any = [];
  /**
   * @param str string not camelCase
   * @returns string converted to camelCase
   */
  const camelize = (str: string) => {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
      if (+match === 0) return ''; // or if (/\s+/.test(match)) for white spaces
      return index === 0 ? match.toLowerCase() : match.toUpperCase();
    });
  };

  /**
   * Takes all fields from props, makes them camelCase and saves to array
   */
  fields?.forEach((element: any) => {
    fieldsArray.push(camelize(element.field));
  });

  const mappedFieldsToObject = fieldsArray.reduce(
    // eslint-disable-next-line no-sequences
    (acc: any, curr: any) => ((acc[curr] = ''), acc),
    {}
  );

  const allFieldsShape = {
    emailAddress: {
      yup: Yup.string().email('Invalid email address format'),
      required: 'Email is required',
    },
    phoneNumber: {
      yup: Yup.string().min(10, 'Phone number must be 10 digits'),
      required: 'Number is required',
    },
    firstName: {
      yup: Yup.string().min(2, 'First name must be 3 characters at minimum'),
      required: 'name is required',
    },
    lastName: {
      yup: Yup.string().min(2, 'Last name must be 3 characters at minimum'),
      required: 'name is required',
    },
    profession: {
      yup: Yup.string(),
      required: 'Profession is required',
    },
    specialty: {
      yup: Yup.string(),
      required: 'Specialty is required',
    },
    yearsOfExperience: {
      yup: Yup.string().min(1, 'At minimum 1 year'),
      required: 'name is required',
    },
    password: {
      yup: Yup.string().min(3, 'Password must be 3 characters at minimum'),
      required: 'password is required',
    },
  };

  const validationSchema = Object?.fromEntries(
    // eslint-disable-next-line array-callback-return
    fields?.map(({ field, required }: { field: string; required: boolean }) => {
      const fieldName = camelize(field);
      if (keys(allFieldsShape).includes(fieldName)) {
        const fromShape = toPairs(allFieldsShape).filter(
          (pair) => pair[0] === fieldName
        );
        return [
          fieldName,
          required
            ? fromShape[0][1].yup.required(fromShape[0][1].required)
            : fromShape[0][1].yup,
        ];
      }
    })
  );

  return (
    <div className={styles.DynamicForm} data-testid="DynamicForm">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <Formik
              initialValues={{ ...mappedFieldsToObject }}
              validationSchema={Yup.object(validationSchema)}
              onSubmit={(values) => {
                Object.keys(values).forEach((key) => {
                  if (values[key] === '') {
                    delete values[key];
                  }
                });
              }}
            >
              {({ touched, errors, values }) => (
                <Form>
                  {fieldsArray.map((field: string, index: number) => {
                    return (
                      <React.Fragment key={field}>
                        <div className={styles.InputWrapper}>
                          <Field
                            key={field + index}
                            id={`formInput_${field}`}
                            name={field}
                            placeholder={fields[index].field}
                            className={`mt-2 form-control
                            ${
                              touched.field && errors.field ? 'is-invalid' : ''
                            }`}
                            required={fields[index].required}
                          />
                          <ErrorMessage
                            component="div"
                            name={field}
                            className="invalid-feedback"
                          />
                        </div>
                      </React.Fragment>
                    );
                  })}
                  <button
                    type="submit"
                    className="btn btn-primary btn-block mt-4"
                  >
                    Submit
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
