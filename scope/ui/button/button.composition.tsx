import React from 'react';
import { Button } from './button';

export const BasicButton = () => (
  <Button text="hello from Button" />
);

export const PrimaryButton = () => (
  <Button text="hello from Primary Button" importance="primary" />
);

export const SecondaryButton = () => (
  <Button text="hello from Secondary Button" importance="secondary" />
);
