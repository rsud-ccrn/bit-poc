import React from 'react';
import styles from './card.module.scss';
import { Button } from '@company/scope.ui.button';

export type CardProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string;
  /**
   * text for button
   */
  buttonText: string;
};

export function Card({ text, buttonText="click here" }: CardProps) {
  return (
    <div className={styles.card}>
      <h2>{text}</h2>
      <Button importance="primary" text={buttonText} />
    </div>
  );
}
