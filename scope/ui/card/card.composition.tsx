import React from 'react';
import { Card } from './card';

export const BasicCard = () => (
  <Card buttonText="Click me" text="hello from Card" />
);
